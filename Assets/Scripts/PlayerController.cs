﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public enum ActionPlayer{notYetSelected, Attack, DamageSpell, HealSpell};
	
	public int hitpoints;
	
	public int magicPoints;
	public int damageSpellCost;
	public int healSpellCost;

	public int attackPower;
	public int damageSpellPower;
	public int healPower;

	public ActionPlayer getAction()
	{
		if (Input.GetKeyDown(KeyCode.A))
			return ActionPlayer.Attack;
		if (Input.GetKeyDown(KeyCode.D))
			return ActionPlayer.DamageSpell;
		if (Input.GetKeyDown(KeyCode.H))
			return ActionPlayer.HealSpell;
		else
			return ActionPlayer.notYetSelected;
	}
}
