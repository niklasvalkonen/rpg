﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {
    
    public float range = 2.0f;
    public GameObject selected = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	    
	}


    void OnGUI()
    { 
        int numChildren = transform.childCount;
        for (int i = 0; i < numChildren; i++)
        {
            Transform child = transform.GetChild(i);
            string itemName = child.gameObject.GetComponent<Item>().name;
            if (GUI.Button(new Rect(0 + i * 100, 0, 100, 50), itemName))
            {
                selected = child.gameObject;
            }
        }
    }
    
}
