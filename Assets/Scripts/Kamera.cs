﻿using UnityEngine;
using System.Collections;

public class Kamera : MonoBehaviour
{
    public Vector3 paikka;
    public Transform Player;
    public Transform PlayerSpawnPoint;
    private GameObject PlayerClone;

    void Start()
    {
        GameObject[] Tag = GameObject.FindGameObjectsWithTag("Player");
        int PlayerCount = Tag.Length;
        if (PlayerCount < 1)
        {
            Instantiate(Player, PlayerSpawnPoint.position, PlayerSpawnPoint.rotation);
        }
    }

    void Update()
    {

        PlayerClone = GameObject.Find("Peasant 1(Clone)");
        transform.position = PlayerClone.transform.position + paikka;

    }

}
